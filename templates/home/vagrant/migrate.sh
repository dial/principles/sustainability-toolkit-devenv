set -x
PGDATABASE=${PGDATABASE} \
PGUSER=${PGUSER} \
PGPASSWORD=${PGPASSWORD} \
PGHOST=${PGHOST} \
PGPORT=${PGPORT} \
yarn --cwd sustainability-toolkit-api migrate
