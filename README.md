# Sustainability Toolkit Development Environment

This repository makes it easy to set up a local development environment for the Sustainability Toolkit project.

This repository borrows heavily from [openTechStrategies/torque-devenv](github.com/openTechStrategies/torque-devenv/), but it is not a fork. That is because the purpose of these scripts (configuration of a development environment) and the differences between the projects mean there will never be upstream interaction.

⚠️ **If you are using an ARM-based development environment** (such as Apple Silicon computers), this setup will not work for you. You should skip down to the [Development on ARM-based hardware](#development-on-arm-based-hardware) section for how to setup the environment manually.

## Usage

Follow these steps to get a development environment.

1. Install dependencies: [Vagrant](https://www.vagrantup.com/downloads) and [Virtualbox](https://www.virtualbox.org/wiki/Downloads).

2. Install [VirtualBox Guest Additions](https://www.virtualbox.org/manual/ch04.html) to support mounting shared folders:
   ```sh
   vagrant plugin install vagrant-vbguest
   ```

3. Create *sibling* directories to `sustainability-toolkit-devenv`:
   * `sustainability-toolkit`: a clone of the [sustainability-toolkit repository](https://gitlab.com/dial/principles/sustainability-toolkit).
   * `sustainability-toolkit-api`: a clone of the [sustainabilit-toolkit-api repository](https://gitlab.com/dial/principles/sustainability-toolkit-api).

   Your directory structure should look something like this:
   ```
   - sustk
   | - sustainability-toolkit
   | - sustainability-toolkit-api
   | - sustainability-toolkit-devenv
   ```

4. Run `vagrant up`

This will build the machine and run the configuration. The final output will expose what the IP address of your guest machine is. This is dynamic and changes frequently between restarts.

## Viewing logs

Service logs for both services are available **in the VM** at `/var/log/supervisor/`.

## Using the development environment

1. Access the toolkit by opening `http://localhost` or `http://{GUEST_IP}/`.
2. Interact with the API by opening `http://localhost/api` or `http://{GUEST_IP}/api`.

⚠️ It may take a few seconds after running `vagrant up` for these urls to work.

## Tips

* To stop the virtual machine, run `vagrant halt`
* To SSH into the virtual machine, run `vagrant ssh`
* If you want to re-run the contents of the Vagrantfile, reprovision it by running `vagrant up --provision`
* If you really want to start completley fresh, run `vagrant destroy`

# Development on ARM-based hardware

If you develop on ARM-based hardware, such as Apple Silicon-based Macs, the environment above will not work. VirtualBox explicitly doesn't support ARM, and Apple's Rosetta 2 engine is unable to translate.

Instead, you will need to manually setup and start each respective environment and start a local HTTP server proxied to each service accordingly.

The following instructions are for a simple [Nginx](https://www.nginx.com)-based proxy setup. You should adjust them accordingly if you want to use a different platform.

1. Clone and setup the [Toolkit](https://gitlab.com/dial/principles/sustainability-toolkit) and [API](https://gitlab.com/dial/principles/sustainability-toolkit-api) repositories.

2. Install, setup, and start Nginx.

3. Create and enable an Nginx site configuration file with the following contents:

   ```
   server {
     listen 80;
     server_name localhost;
     
     location / {
       proxy_set_header   X-Forwarded-For $remote_addr;
       proxy_set_header   Host $http_host;
       proxy_pass         http://localhost:3000;
     }
     
     location /api {
       return 302 /api/;
     }
     
     location /api/ {
       proxy_set_header   X-Forwarded-For $remote_addr;
       proxy_set_header   Host $http_host;
       proxy_redirect     ~^/(.*)$ /api/$1;
       proxy_pass         http://localhost:3001/;
     }
   }
   ```

   ⚠️ Be sure to change the port numbers above if you aren't using the respective project defaults.

4. Restart/reload Nginx.

5. Now, when both the Toolkit and API are running (via their respective start commands), `http://localhost` should proxy to the Toolkit and `http://localhost/api` should proxy to the API, allowing you to test the full feature-set of the application.
