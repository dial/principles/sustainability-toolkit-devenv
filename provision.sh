#!/usr/bin/env bash
echo "Running configuration script"
export DEBIAN_FRONTEND=noninteractive

echo "US/Eastern" | sudo tee /etc/timezone

echo "Install essential software pacakges"
apt-get -qq update
apt-get -qq install \
	postgresql \
	supervisor \

echo "Install NodeJS 16.x"
curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash - && sudo apt-get install -y nodejs
npm install -g yarn

echo "Install web server"
apt-get -qq install -y nginx

echo "Setting up Postgres"
export PGDATABASE=sustk
export PGUSER=sustk_user
export PGPASSWORD="$(LC_CTYPE=C tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=32 count=1 2>/dev/null;echo)"
export PGHOST=127.0.0.1
export PGPORT=5432
export API_PORT=3001
export SITE_PORT=3000
export TOOLKIT_DOMAIN=localhost
sudo -u postgres createdb $DB_NAME
sudo -u postgres psql -c "create user $DB_USERNAME password '$DB_PASSWORD'"

echo "Setting up the Sutainability Toolkit API"
cd /home/vagrant/sustainability-toolkit-api
yarn install

echo "Setting up the Sutainability Toolkit"
cd /home/vagrant/sustainability-toolkit
yarn install

echo "Setting up Supervisor"
envsubst < $TEMPLATES_PATH/etc/supervisor/conf.d/sustainability-toolkit-api.conf > /etc/supervisor/conf.d/sustainability-toolkit-api.conf
envsubst < $TEMPLATES_PATH/etc/supervisor/conf.d/sustainability-toolkit.conf > /etc/supervisor/conf.d/sustainability-toolkit.conf
supervisorctl reload

echo "Setting up nginx"
rm /etc/nginx/sites-enabled/default
envsubst '$SITE_PORT, $API_PORT, $TOOLKIT_DOMAIN' < $TEMPLATES_PATH/etc/nginx/sites-available/sustainability-toolkit > /etc/nginx/sites-available/sustainability-toolkit
ln -s /etc/nginx/sites-available/sustainability-toolkit /etc/nginx/sites-enabled/sustainability-toolkit
service nginx reload

echo "Setting up migration script"
envsubst < $TEMPLATES_PATH/home/vagrant/migrate.sh > /home/vagrant/migrate.sh
chmod 755 /home/vagrant/migrate.sh
